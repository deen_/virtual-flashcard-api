# Virtual Flashcard API

VF is an application to recreate a conventional flashcard into a virtual learning tool.

## Install

```
git clone https://deen_@bitbucket.org/deen_/virtual-flashcard-api.git
```


## Run the App

```
npm run dev
```

## Usage

Refer to this link for the API Documentation 
https://documenter.getpostman.com/view/14607340/UVsEVp3y

