const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const User = require('../../src/models/user')
const Deck = require('../../src/models/deck')
const Card = require('../../src/models/card')

const userOneId = new mongoose.Types.ObjectId()
const userOne = {
    _id: userOneId,
    name: 'Deen Marfa',
    email: 'deen@example.com',
    password: '1234567',
    tokens: [{
        token: jwt.sign({ _id: userOneId }, process.env.JWT_SECRET)
    }]
}

const deckOneId = new mongoose.Types.ObjectId()
const deckOne = {
    _id: deckOneId,
    description: 'Hiragana Syllabary',
    public: false,
    owner: userOneId
}

const deckTwoId = new mongoose.Types.ObjectId()
const deckTwo = {
    _id: deckTwoId,
    description: 'Katakana Syllabary',
    public: false,
    owner: userOneId
}

const deckThreeId = new mongoose.Types.ObjectId()
const deckThree = {
    _id: deckThreeId,
    description: 'Asian Countries',
    public: true,
    owner: userOneId
}

const cardOneId = new mongoose.Types.ObjectId()
const cardOne = {
    _id: cardOneId,
    front: 'あa',
    back: 'aa',
    parent_deck: deckOneId
}

const setupDatabase = async () => {
    await User.deleteMany()
    await new User(userOne).save()

    await Deck.deleteMany()
    await new Deck(deckOne).save()
    await new Deck(deckTwo).save()
    await new Deck(deckThree).save()
    
    await Card.deleteMany()
    await new Card(cardOne).save()
}

module.exports = {
    userOneId,
    userOne,
    deckOne,
    cardOne,
    setupDatabase
}