const request = require('supertest')
const app = require('../src/app')
const Deck = require('../src/models/deck')
const {userOne, deckOne, setupDatabase } = require('./fixtures/db')

beforeEach(setupDatabase)

test('Should create a deck when authenticated', async () => {
    const response = await request(app)
        .post('/decks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: 'Hiragana Syllabary',
            public: true
        }).expect(201)

    const deck = await Deck.findById(response.body._id)
    expect(deck).not.toBeNull()
})

test('Should not be able to create deck when unauthenticated', async () => {
    await request(app)
        .post('/decks')
        .send({
            description: 'Hiragana Syllabary',
            public: true
        }).expect(401)
})

test('Should get public decks when unauthenticated', async () => {
    const response = await request(app)
        .get('/decks')
        .send()
        .expect(200)
    expect(response.body.length).toEqual(1)
})

test('Should get public and own decks when authenticated', async () => {
    const response = await request(app)
        .get('/decks/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toEqual(3)
})

test('Should update own decks', async () => {
    await request(app)
        .patch(`/decks/${deckOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: "Updated description"
        })
        .expect(200)
})

test('Should not be able to update other decks', async () => {
    await request(app)
        .patch(`/decks/${deckOne._id}`)
        .set('Authorization', `Bearer SOMEOTHERTOKEN`)
        .send({
            description: "Updated description"
        })
        .expect(401)
})

test('Should delete own decks', async () => {
    await request(app)
        .delete(`/decks/${deckOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})

test('Should not be able to delete other decks', async () => {
    await request(app)
        .delete(`/decks/${deckOne._id}`)
        .set('Authorization', `Bearer SOMEOTHERTOKEN`)
        .send()
        .expect(401)
})