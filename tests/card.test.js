const request = require('supertest')
const app = require('../src/app')
const Card = require('../src/models/card')
const { userOne, deckOne, cardOne, setupDatabase } = require('./fixtures/db')

beforeEach(setupDatabase)

test('Should create a card when authenticated', async () => {
    const response = await request(app)
        .post(`/decks/${deckOne._id}/cards`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            front: 'あ',
            back: 'a'
        }).expect(201)

    const card = await Card.findById(response.body._id)
    expect(card).not.toBeNull()
})

test('Should not be able to create deck when unauthenticated', async () => {
    await request(app)
        .post(`/decks/${deckOne._id}/cards`)
        .send({
            front: 'あ',
            back: 'a'
        }).expect(401)
})

test('Should get cards', async () => {
    const response = await request(app)
        .get(`/decks/${deckOne._id}/cards`)
        .send()
        .expect(200)
    expect(response.body.length).toEqual(1)
})

test('Should update own cards', async () => {
    await request(app)
        .patch(`/decks/${deckOne._id}/cards/${cardOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            back: 'A'
        })
        .expect(200)
})

test('Should not be able to update other decks', async () => {
    await request(app)
        .patch(`/decks/${deckOne._id}/cards/${cardOne._id}`)
        .set('Authorization', `Bearer SOMEOTHERTOKEN`)
        .send({
            back: 'A'
        })
        .expect(401)
})

// test('Should delete own decks', async () => {
//     await request(app)
//         .delete(`/decks/${deckOne._id}/cards/${cardOne._id}`)
//         .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
//         .send()
//         .expect(200)
// })

test('Should not be able to delete other decks', async () => {
    await request(app)
        .delete(`/decks/${deckOne._id}/cards/${cardOne._id}`)
        .set('Authorization', `Bearer SOMEOTHERTOKEN`)
        .send()
        .expect(401)
})