const express = require('express')
const cors = require('cors')
require('./db/mongoose')

const userRouter = require('./routers/user')
const deckRouter = require('./routers/deck')
const cardRouter = require('./routers/card')

const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));  

app.use(cors());

app.get('/', (req, res) => {
    res.status(200).send({
        name: "Virtual Flashcard API",
        version: "1.0.0",
        author: "deen♥"
    })    
})
app.use(userRouter)
app.use(deckRouter)
app.use(cardRouter)

module.exports = app