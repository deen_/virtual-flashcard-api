const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Deck = require('../models/deck')

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '')
        const decoded = jwt.verify(token, process.env.JWT_SECRET)
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })
    
        if (!user) {
            throw new Error()
        }

        const deck_id = req.params.id
        const owner = await Deck.findOne({ _id: deck_id, owner: user._id })

        if  (!owner) {
            res.status(500).send({ error: 'Cannot proceed! You are not the owner of the deck.' })
        }

        req.user = user
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate.' })
    }
}

module.exports = auth