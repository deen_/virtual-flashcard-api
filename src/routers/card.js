const express = require('express')
const Card = require('../models/card')
const owner_auth = require('../middleware/owner_auth')
const router = new express.Router()

router.post('/decks/:id/cards', owner_auth, async (req, res) => {
    const card = new Card({
        ...req.body,
        parent_deck: req.params.id
    })

    try {
        await card.save()
        res.status(201).send(card)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/decks/:id/cards', async (req, res) => {
    const _id = req.params.id
    
    try {
        const cards = await Card.find({ parent_deck: _id })

        if (!cards) {
            return res.status(404).send()
        }

        res.send(cards)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/decks/:id/cards/:c_id', async (req, res) => {
    const _id = req.params.c_id

    try {
        const card = await Card.findOne({ _id })

        if (!card) {
            return res.status(404).send()
        }

        res.send(card)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.patch('/decks/:id/cards/:c_id', owner_auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['front', 'back']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return res.status(400).send('error: Invalid updates')
    }

    try {

        const card = await Card.findOne({ _id: req.params.c_id })

        if (!card) {
            return res.status(404).send()
        }

        updates.forEach((update) => card[update] = req.body[update])
        await card.save()

        res.send(card)
    } catch (e) {
        res.status(400).send(e)
    }
})


router.delete('/decks/:id/cards/:c_id', owner_auth, async(req, res) => {
    try {
        const card = await Card.findOneAndDelete({ _id: req.params.c_id })

        if (!card) {
            return res.status(404).send()
        }

        res.send(card)
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router