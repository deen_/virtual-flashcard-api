const express = require('express')
const Deck = require('../models/deck')
const User = require('../models/user')
const auth = require('../middleware/auth')
const router = new express.Router()

router.post('/decks', auth, async (req, res) => {
    const deck = new Deck({
        ...req.body,
        owner: req.user._id
    })

    try {
        await deck.save()
        deck.cards = {}
        res.status(201).send(deck)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/decks/me', auth, async (req, res) => {
    try {
        let result = Array();   
        let decks = [];
        
        if (req.query.includePublic == 'true') {
            decks = await Deck.find({ $or: [ {public:true}, {owner: req.user._id} ]}).populate(
                { path: 'cards', select: 'front back' }
            );
        } else {
            decks = await Deck.find({ owner: req.user._id }).populate(
                { path: 'cards', select: 'front back' }
            );
        }
            
        decks.forEach(d => {
            result.push({
                _id: d._id, 
                description: d.description, 
                public: d.public, 
                cards: d.cards
            })
        })

        res.send(result)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.get('/decks', async (req, res) => {
    try {
        var result = Array();   
        const decks = await Deck.find({ public: true }).populate(
            { path: 'cards', select: 'front back' }
        ).populate('creator')
            
        decks.forEach(d => {
            result.push({
                _id: d._id, 
                description: d.description, 
                public: d.public, 
                creator: d.creator.name,
                cards: d.cards
            })
        })

        res.status(200).send(result)
    } catch (e) {
        console.log(e)
        res.status(500).send(e)
    }
})

router.get('/decks/:id', async (req, res) => {
    const _id = req.params.id

    try {
        const deck = await Deck.findOne({ _id })

        if (!deck) {
            return res.status(404).send()
        }

        res.send(deck)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.patch('/decks/:id', auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['description', 'public']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return res.status(400).send('error: Invalid updates')
    }

    try {
        const deck = await Deck.findOne({ _id: req.params.id, owner: req.user._id }).populate(
            { path: 'cards', select: 'front back' }
        );

        if (!deck) {
            return res.status(404).send()
        }

        updates.forEach((update) => deck[update] = req.body[update])
        await deck.save()

        res.send(deck)
    } catch (e) {
        res.status(400).send(e)
    }
})


router.delete('/decks/:id', auth, async(req, res) => {
    try {
        const deck = await Deck.findOneAndDelete({ _id: req.params.id, owner: req.user._id })

        if (!deck) {
            return res.status(404).send()
        }

        res.send(deck)
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router