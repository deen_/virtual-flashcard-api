const fs = require('fs')
require('../db/mongoose')

const User = require('../models/user')
const Deck = require('../models/deck')
const Card = require('../models/card')

const users = JSON.parse(
    fs.readFileSync(`${__dirname}/_data/users.json`, "utf-8")
)
const decks = JSON.parse(
    fs.readFileSync(`${__dirname}/_data/decks.json`, "utf-8")
)
const cards = JSON.parse(
    fs.readFileSync(`${__dirname}/_data/cards.json`, "utf-8")
)

const importData = async() => {
    try {
        console.log("Seeding users...");
        await User.deleteMany();
        for(const data of users ) {
            const user = new User(data)
            await user.save()
        }

        console.log("Seeding decks...");
        await Deck.deleteMany();
        await Deck.insertMany(decks);

        console.log("Seeding cards...");
        await Card.deleteMany();
        await Card.insertMany(cards);

        console.log("Seeding completed!");
        process.exit();
    } catch (e) {
        console.log(e);
    }
}

importData();