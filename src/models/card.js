const mongoose = require('mongoose')

const cardSchema = new mongoose.Schema({
    front: {
        type: String,
        required: true,
        trim: true
    }, 
    back: {
        type: String,
        required: true,
        trim: true
    },
    parent_deck: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Deck'
    }
}, {
    timestamps: true
})

const Card = mongoose.model('Card', cardSchema)

module.exports = Card