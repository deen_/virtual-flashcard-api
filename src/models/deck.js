const mongoose = require('mongoose')

const deckSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true,
        trim: true
    }, 
    public: {
        type: Boolean,
        default: false
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
}, {
    timestamps: true
})

deckSchema.virtual('cards', {
    ref: 'Card',
    localField: '_id',
    foreignField: 'parent_deck'
})

deckSchema.virtual('creator', {
    ref: 'User',
    localField: 'owner',
    foreignField: '_id',
    justOne: true
})

const Deck = mongoose.model('Deck', deckSchema)

module.exports = Deck